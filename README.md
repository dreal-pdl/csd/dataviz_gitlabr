# dataviz_gitlabr

la connexion est prevue pour se connecter au site "https://gitlab-forge.din.developpement-durable.gouv.fr/"

## But du projet :

le but principal est de d'obtenire un tableau de bord des projets GitLab-Forge à partir d'un groupe de projet et d'en extraire des données afin de réaliser un tableau de bord des suivie des différent projet mener par la DREAL Pays De Le Loire Datalab.




## Débuter

- [ ] premier point important modifier son Renviron avec la commande  suivante pour editer sa connexion
```
usethis::edit_r_environ()

```
et ensuite lorsque la page est ouverte il faut copier/coller la commande suivante dans son Renviron (remplacer les guillmet avec le token creés sur gitlab-Forge)
```
GITLAB_COM_TOKEN='votre token doit etre copier ici (sans guillmet)'

```

Puis redémarrer votre session.

